<?php

$countries = [
    "id" => new MongoId(),
    "nom" => "france",
];

$region = [
    "0" => [
        "id" => new MongoId(),
        "nom" => "lorraine",
        "pays" => "France"
    ]
];

$cities = [
    "0" => [
        "id" => new MongoId(),
        "nom" => "Nancy",
        "region" => "Lorraine",
        "pays"
    ],
];

$categ = [
    "0" => [
        "idCateg" => new MongoId(),
        "categ" => "bar"
    ]
];

$events = [
    "0" => [
        "id" => new Mongoid(),
        "id_marker" => "12",
        "name" => "la soirée du siecle",
        "date" => "DD/MM/YYYY",
        // 0 if not parent event
        "id_parent" => "0",
        "comments" => [
            "0" => [
                "date" => "DD/MM/YYYY",
                "author" => "user_toto",
                "text" => "blabla c'etait nul"
            ],
        ]
    ]
];

$markers = [
    "id" => new MongoId(),
    "positionX" => "52.03",
    "positionY" => "53.04",
    "events" => [
        "0" => [
            "id_event" => "12"
        ]

    ]
]; 