<?php



$mng = new MongoDB\Driver\Manager("mongodb://localhost:27017");

function convertMongoIds(array &$array){
    foreach ($array[0] as &$element){
        if (is_object($element) && get_class($element) == 'MongoDB\BSON\ObjectID'){
            $element = (string) $element;
        }
    }
}


if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $jsonString = file_get_contents("php://input");

    $jsonString = json_decode($jsonString);


    $note = get_object_vars($jsonString[0]);
    $ref = get_object_vars($jsonString[1]);


    $ref = substr($ref['value'], 0, -1);//remove the last ' " '


    $obj = json_decode($ref);





    $criteria = array(
        '_id' => new MongoDB\BSON\ObjectID($obj->{'_id'})
    );
    $document = array(
        '$set' => array(
            'country' => [
                '_id' => new MongoDB\BSON\ObjectID($obj->{'country'}->{'_id'}),
                'name' => $obj->{'country'}->{'name'},
                'city' => [
                    '_id' => new MongoDB\BSON\ObjectID($obj->{'country'}->{'city'}->{'_id'}),
                    'name' => $obj->{'country'}->{'city'}->{'name'},
                    'marker' => [
                        '_id' => new MongoDB\BSON\ObjectID($obj->{'country'}->{'city'}->{'marker'}->{'_id'}),
                        'name' => $obj->{'country'}->{'city'}->{'marker'}->{'name'},
                        'street' => $obj->{'country'}->{'city'}->{'marker'}->{'street'},
                        'lat' => $obj->{'country'}->{'city'}->{'marker'}->{'lat'},
                        'lng' => $obj->{'country'}->{'city'}->{'marker'}->{'lng'},
                        'countNotation' => $obj->{'country'}->{'city'}->{'marker'}->{'countNotation'}+1,
                        'notation' => $obj->{'country'}->{'city'}->{'marker'}->{'notation'}+$note['value'],
                        'notationEdit' => false,
                    ]
                ]
            ]
        ),
    );
    $updateOptions = array(
        "multi" => true,
    );

    $bulk = new MongoDB\Driver\BulkWrite;
    $bulk->update(
        $criteria,
        $document,
        $updateOptions
    );

    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 100);

    $manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    $result = $manager->executeBulkWrite("testdb.event", $bulk, $writeConcern);





}