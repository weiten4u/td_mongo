<?php

$mng = new MongoDB\Driver\Manager("mongodb://localhost:27017");


function convertMongoIds(array &$array){


if (is_array($array[0]) || is_object($array[0]))
{
    foreach ($array[0] as &$element){

        if (is_object($element) && get_class($element) == 'MongoDB\BSON\ObjectID'){
            $element = (string) $element;
        }else{
            $temp = array($element);
            convertMongoIds($temp);

        }

    }
}


}


if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $jsonString = file_get_contents("php://input");
    //file_put_contents("test.txt", $jsonString);

    $requete = get_object_vars(json_decode($jsonString));



    $filter = ['name' => $requete['country']];
    $options = [];
    $query = new MongoDB\Driver\Query($filter, $options);
    $cursor = $mng->executeQuery('testdb.countries', $query); // $mongo contains the connection object to MongoDB
    $country_id = 0;

    foreach ($cursor as $res) {
        $country_id = new MongoDB\BSON\ObjectID((string) get_object_vars($res)['_id']);
    }

    $filter = ['value' => $requete['city']];
    $options = [];
    $query = new MongoDB\Driver\Query($filter, $options);
    $cursor = $mng->executeQuery('testdb.cities', $query); // $mongo contains the connection object to MongoDB
    $city_id = 0;

    foreach ($cursor as $res) {
        $city_id = new MongoDB\BSON\ObjectID((string) get_object_vars($res)['_id']);
    }

    //echo $country_id;
    //echo $city_id;

    /*
     * On cree les entites si elles n'existent pas encore
     * */
    if($country_id == 0){

        $country_id = new MongoDB\BSON\ObjectID();
        $country = [
            '_id' => $country_id,
            'name' => $requete['country'],
        ];
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->insert($country);
        $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 100);
        $result = $mng->executeBulkWrite('testdb.countries', $bulk, $writeConcern);

        printf("Inserted %d country(ies)\n", $result->getInsertedCount());
    }

    if($city_id == 0){

        $city_id = new MongoDB\BSON\ObjectID();
        $city = [
            '_id' => $city_id,
            'name' => $requete['city'],
        ];
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->insert($city);
        $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 100);
        $result = $mng->executeBulkWrite('testdb.cities', $bulk, $writeConcern);

        printf("Inserted %d city(ies)\n", $result->getInsertedCount());
    }

    $now = new DateTime();
    $now->format('d-m-Y H:i:s');
    $event = [
        'timestamp' =>  $now,
        'country' => [
            '_id' => $country_id, 
            'name' => $requete['country'],
            'city' => [
                '_id' => $city_id,
                'name' => $requete['city'],
                'marker' => [
                    '_id' => new MongoDB\BSON\ObjectID(),
                    'name' => $requete['name'],
                    'street' => $requete['street'],
                    'lat' => $requete['lat'],
                    'lng' => $requete['lng'],
                    'type' => $requete['type'],
                    'description' => trim($requete['description']),
                    'date' => $requete['date'],
                    'notation' => 0,
                    'countNotation' => 0,
                    'notationEdit' => false,
                    'comments' => [
                        'total' => 0
                    ]
                ]
            ]
        ]
    ];


    $bulk = new MongoDB\Driver\BulkWrite;
    $bulk->insert($event);
    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 100);
    $result = $mng->executeBulkWrite('testdb.event', $bulk, $writeConcern);

    printf("Inserted %d document(s)\n", $result->getInsertedCount());


}


if ($_SERVER['REQUEST_METHOD'] === 'GET') {


    $events = [];
    $object_id = 0;

    //$manager = new \MongoDB\Driver\Manager("mongodb://localhost:27017");

    //$filter = ['value' => 'Nancy'];
    $filter = [];
    $options = ['sort' => ['timestamp' => -1]];
    $query = new MongoDB\Driver\Query($filter, $options);
    $rows = $mng->executeQuery('testdb.event', $query); // $mongo contains the connection object to MongoDB
    foreach($rows as $element){

       array_push($events,$element);

    }



    convertMongoIds($events);
    echo json_encode($events);



}



?>
