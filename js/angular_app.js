/**
 * Created by David on 06/01/2017.
 */
var app = angular.module('markers_event', []);

app.controller("map", ['$scope', '$rootScope', '$http',
    function ($scope, $rootScope, $http) {

        var map;
        var list = [];
        $scope.addNotationEdit = 'add';
        $scope.addCommentdit = 'add';


        $scope.initMap = function () {
            var france = {lat: 46.227638, lng: 2.213749000000007};
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 6,
                center: france

            });
            $scope.displayMarker();

        };

        angular.element(document).ready(function () {
            $scope.initMap();
        });


        $scope.displayMarker = function () {

            $http.get('events.php').then(function (response) {
                $rootScope.list = response.data;


                response.data.forEach(function (key, value) {

                    //console.log(key.country.city.name);


                    var point = {lat: key.country.city.marker.lat, lng: key.country.city.marker.lng};
                    var marker = new google.maps.Marker({
                        position: point,
                        map: map,

                        title: key.country.city.marker.name
                    });


                });
                //console.log(response);

            });
        };


        $scope.save = function (htmlForm) {

            var data = $("#details :input").serializeArray();

            var event = {};
            var data_precision = data[0].value.split(',').length;
            var modalId = "#myModal";

            event.name = data[1].value;



            if (data_precision == 3) {
                event.street = data[0].value.split(',')[0];
                event.city = data[0].value.split(',')[1];
                event.country = data[0].value.split(',')[2];
            } else if (data_precision == 2) {
                event.city = data[0].value.split(',')[0];
                event.country = data[0].value.split(',')[1];
            } else if (data_precision == 1) {
                event.country = data[0].value.split(',')[0];
            }

            var location = event.country + "," + event.city;

            if (event.street != null) {
                location += "," + event.street;

            }

            event.type = data[2].value;
            event.description = data[3].value;
            event.date = data[4].value;


            //normalment tout est ok c'est un plus propre même si je pige pas trop les tableau sont ordonnées pareil
            $http.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + location).then(function (response) {
                console.log(response.data);

                response.data.results.forEach(function (k, v) {
                    event.lat = k.geometry.location.lat;
                    event.lng = k.geometry.location.lng;

                    $http.post("events.php", JSON.stringify(event), []).then(
                        function (data, status, headers, config) {
                            $(modalId).modal('hide');
                            $scope.initMap();
                        }
                    );
                });

            });

        };

        $scope.move = function (element) {

            //console.log(element);

            var point = {lat: element.country.city.marker.lat, lng: element.country.city.marker.lng};
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: point
            });

            $scope.displayMarker();
        };

        $scope.addNotation = function (l) {
            if ($scope.addNotationEdit === 'add') {
                l.country.city.marker.notationEdit = true;
                $scope.addNotationEdit = 'edit';
            }
            $scope.addNotationEdit = 'add';
        };


        $scope.addNotationHide = function (l) {
            l.country.city.marker.notationEdit = false;
        };


        $scope.saveNotation = function () {
           // console.log('coucou');
            //console.log($("#notation :input"));
            var $post = $("#notation :input").serializeArray();

            // l.country.city.marker.notation = $post[0].value;

            $http.post("notation.php", JSON.stringify($post), []).then(
                function (data, status, headers, config) {
                    console.log(data.config.data);
                }
            );
        };




        $scope.addComment = function (l) {
            if ($scope.addCommentdit === 'add') {
                l.country.city.marker.commentEdit = true;
                $scope.addCommentdit = 'edit';
            }
            $scope.addCommentdit = 'add';
        };


        $scope.addCommentHide = function (l) {
            l.country.city.marker.commentEdit = false;
        };

        $scope.saveComment = function () {

            var $post = $("#comments :input").serializeArray();

            console.log($post);

            $http.post("comments.php", JSON.stringify($post), []).then(
                function (data, status, headers, config) {
                    console.log(data.config.data);
                }
            );
        };

    }

]);

