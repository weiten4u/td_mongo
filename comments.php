<?php


$mng = new MongoDB\Driver\Manager("mongodb://localhost:27017");


if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $jsonString = file_get_contents("php://input");
    $jsonString = json_decode($jsonString);


    $comment = get_object_vars($jsonString[0]);
    $ref = get_object_vars($jsonString[1]);


    $ref = substr($ref['value'], 0, -1);//remove the last ' " '
    $obj = json_decode($ref);


    $tab_comment_id = $obj->{'country'}->{'city'}->{'marker'}->{'comments'}->{'comments_id'};
    $tmp = (array)$obj->{'country'}->{'city'}->{'marker'}->{'comments'};
    $tmp['total'] = $tmp['total'] + 1;
    $tmp['comment_' . $tmp['total']] = $comment['value'];


    /**
     * Update the current event with the id of the new comment
     */
    $criteria = array(
        '_id' => new MongoDB\BSON\ObjectID($obj->{'_id'})
    );
    $document = array(
        '$set' => array(
            'country' => [
                '_id' => new MongoDB\BSON\ObjectID($obj->{'country'}->{'_id'}),
                'name' => $obj->{'country'}->{'name'},
                'city' => [
                    '_id' => new MongoDB\BSON\ObjectID($obj->{'country'}->{'city'}->{'_id'}),
                    'name' => $obj->{'country'}->{'city'}->{'name'},
                    'marker' => [
                        '_id' => new MongoDB\BSON\ObjectID($obj->{'country'}->{'city'}->{'marker'}->{'_id'}),
                        'name' => $obj->{'country'}->{'city'}->{'marker'}->{'name'},
                        'street' => $obj->{'country'}->{'city'}->{'marker'}->{'street'},
                        'lat' => $obj->{'country'}->{'city'}->{'marker'}->{'lat'},
                        'lng' => $obj->{'country'}->{'city'}->{'marker'}->{'lng'},
                        'countNotation' => $obj->{'country'}->{'city'}->{'marker'}->{'countNotation'},
                        'notation' => $obj->{'country'}->{'city'}->{'marker'}->{'notation'},
                        'notationEdit' => false,
                        'commentEdit' => false,
                        'comments' => $tmp

                    ]
                ]
            ]
        ),
    );
    $updateOptions = array(
        "multi" => true,
    );

    $bulk = new MongoDB\Driver\BulkWrite;
    $bulk->update(
        $criteria,
        $document,
        $updateOptions
    );

    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 100);

    $manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    $result = $manager->executeBulkWrite("testdb.event", $bulk, $writeConcern);


    var_dump($result);


}